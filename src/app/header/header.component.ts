import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  items;

  constructor() {
  }

  ngOnInit(): void {
    this.items = [
      {label: 'BOOKS', icon: 'pi pi-list', url: '/books'},
      {label: 'AUTHORS', icon: 'pi pi-users', url: '/authors'}
    ];
  }

}
